package com.ardr.salaryapi.model;

import com.ardr.salaryapi.enums.SalaryPositionStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryResponse {
    private Long id;
    private String name;
    private SalaryPositionStatus position;
    private Double preTex;
    private Double pension;
    private Double health;
    private Double employ;
    private Double accident;
    private Double income;
    private Double texFree;
    private Double afterTex;
}

package com.ardr.salaryapi.model;

import com.ardr.salaryapi.enums.SalaryPositionStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryItem {
    private String name;
    @Enumerated(value = EnumType.STRING)
    private SalaryPositionStatus position;
    private Double preTex;
}

package com.ardr.salaryapi.controller;


import com.ardr.salaryapi.model.SalaryItem;
import com.ardr.salaryapi.model.SalaryRequest;
import com.ardr.salaryapi.model.SalaryResponse;
import com.ardr.salaryapi.service.SalaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/salary")
public class SalaryController {

    private final SalaryService salaryService;

    @PostMapping("/people")
    public String setSalary(@RequestBody SalaryRequest request) {
        salaryService.setSalary(request);
        return ("OK");
    }

    @GetMapping("/all")
    public List<SalaryItem> getSalaries(){
        return salaryService.getSalaries();
    }


    @GetMapping("/detail/{id}")
    public SalaryResponse getSalary(@PathVariable long id){
        return salaryService.getSalary(id);
    }
}

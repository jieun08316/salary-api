package com.ardr.salaryapi.entity;


import com.ardr.salaryapi.enums.SalaryPositionStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.security.SecureRandom;

@Entity
@Getter
@Setter
public class Salary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private SalaryPositionStatus position;

    @Column(nullable = false)
    private Double preTex;

    @Column(nullable = false)
    private Double pension;

    @Column(nullable = false)
    private Double health;

    @Column(nullable = false)
    private Double employ;

    @Column(nullable = false)
    private Double accident;

    @Column(nullable = false)
    private Double income;

    @Column(nullable = false)
    private Double texFree;

    }


package com.ardr.salaryapi.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.swing.*;

@Getter
@AllArgsConstructor
public enum SalaryPositionStatus {

    EMPLOYEE("사원")
    , AN_ASSISTANT_MANAGER("대리")
    , EXAGGERATION("과장")
    , DEPUTY_DIRECTOR("차장")
    , HEAD_OF_DEPARTMENT("부장");


    private final String name;
}

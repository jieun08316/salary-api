package com.ardr.salaryapi.service;


import com.ardr.salaryapi.entity.Salary;
import com.ardr.salaryapi.model.SalaryItem;
import com.ardr.salaryapi.model.SalaryRequest;
import com.ardr.salaryapi.model.SalaryResponse;
import com.ardr.salaryapi.repository.SalaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SalaryService {

    private final SalaryRepository salaryRepository;

    public void setSalary(SalaryRequest request) {
        Salary addData = new Salary();
        addData.setName(request.getName());
        addData.setPosition(request.getPosition());
        addData.setPreTex(request.getPreTex());
        addData.setPension(request.getPension());
        addData.setHealth(request.getHealth());
        addData.setEmploy(request.getEmploy());
        addData.setAccident(request.getAccident());
        addData.setIncome(request.getIncome());
        addData.setTexFree(request.getTexFree());

        salaryRepository.save(addData);
    }

    public List<SalaryItem> getSalaries() {
        List<Salary> originList = salaryRepository.findAll();
        List<SalaryItem> result = new LinkedList<>();
        for (Salary salary : originList) {
            SalaryItem addItem = new SalaryItem();
            addItem.setName(salary.getName());
            addItem.setPosition(salary.getPosition());
            addItem.setPreTex(salary.getPreTex());

            result.add(addItem);
        }
        return result;
    }

    public SalaryResponse getSalary(long id) {


        Salary originData = salaryRepository.findById(id).orElseThrow();
        SalaryResponse response = new SalaryResponse();
        response.setId(originData.getId());
        response.setPosition(originData.getPosition());
        response.setName(originData.getName());
        response.setPension(originData.getPension());
        response.setPreTex(originData.getPreTex());
        response.setHealth(originData.getHealth());
        response.setEmploy(originData.getEmploy());
        response.setAccident(originData.getAccident());
        response.setIncome(originData.getIncome());
        response.setTexFree(originData.getTexFree());
        response.setAfterTex(originData.getPreTex()
                - originData.getPension()
                - originData.getHealth()
                - originData.getEmploy()
                - originData.getAccident()
                - originData.getIncome()
                + originData.getTexFree());

        return response;
    }


}
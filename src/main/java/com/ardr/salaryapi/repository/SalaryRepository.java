package com.ardr.salaryapi.repository;


import com.ardr.salaryapi.entity.Salary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalaryRepository extends JpaRepository<Salary, Long> {
}
